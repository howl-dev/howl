curl -XPUT 'localhost:9200/howldb?pretty' -H 'Content-Type: application/json' -d'
{
    "settings" : {
        "number_of_shards" : 1
    },
    "mappings" : {
        "tweets" : {
            "properties" : {
                "username" : { "type" : "keyword" },
		"property": { "properties": { "likes": { "type" : "integer" }}},
		"retweeted": {"type": "integer"},
		"content": {"type": "text"},
		"timestamp": {"type": "long"}
            }
        }
    }
}
'



var express = require('express');
var router = express.Router();

/* Module Endpoints */
const search = require('./search');

/* Endpoints */
router.post('/search', search.search);

module.exports = router;
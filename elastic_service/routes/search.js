const db = require('../db');
const memcached = require('../sessions');
const helpers = require('../helpers');
const jwt = require('jsonwebtoken');
var bodybuilder = require('bodybuilder');
const ElasticSearch = require('../elastic').ElasticSearch;
const sendErr = helpers.sendErr;

/**
 * Search endpoint. Will query Elastic Search with request's arguments.
 * 
 * Query String, Username (to filter results by), and Following 
 * (to filter if users that follow logged in user) are all optional.
 * 
 * Additional optional args:
 * rank: "interest" or "time" default: "time"
 *  interest: weighing time vs. number of likes and retweets.
 *  time: earliest first.
 *
 * parent: return items made in reply to requested item ID
 * 
 * replies: boolean to include replies. Default: true
 * 
 * hasMedia: return items with media only. Default: false
 * @param {Request} req Request object
 * @param {Response} res Response Object
 */
async function search(req, res) {
    let start_time = req.body.timestamp;
    if (!start_time) {
        start_time = +new Date();
    } else {
        start_time = new Date(0).setUTCSeconds(start_time);
    }

    let limit = req.body.limit || 25;
    if (limit > 100) {
        sendErr(res, 'Limit too high', 400);
        return;
    }

    let payload = undefined;
    let following = req.body.following;
    if (following === undefined || following === null) following = true;
    let jwtCookie = req.cookies.key;

    // if folliwing requested and no cookie, not logged in error
    if (following && !jwtCookie) {
        sendErr(res, 'Not logged in!', 401);
        return;
    }

    // get optional args
    let query_string = req.body.q;
    let req_username = req.body.username;
    let rank = req.body.rank ? req.body.rank : 'interest';
    let parent = req.body.parent;
    let replies = req.body.replies === undefined ? true : req.body.replies;
    let hasMedia = req.body.hasMedia;
    let follows = [];

    // if following, get the list of the users that the logged in user follows
    if (following) {
        try {
            payload = await jwt.verify(jwtCookie, process.env.JWT_SIGN_SECRET);        
            // let chk = await memcached.retrieveSession(payload.key);
            // if (chk != jwtCookie) throw 'Bad cookie';
        } catch (err) {
            res.clearCookie('key');
            sendErr(res, err, 401);
            return;
        }

        // Get the people they are following
        let user = payload.key;
        try {
            follows = await db.Follower.find({followerUsername: user})
                .select('parentUsername').lean().exec();

            let _follows = [];
            for (let i = 0; i < follows.length; i++) {
                _follows.push(follows[i].parentUsername);
            }
            follows = _follows;
        } catch (err) {
            sendErr(res, err);
            return;
        }
    }

    // build the search object for the tweets query
    // minimum object has just a timestamp filter
    let search_arg = {
        index: 'howldb',
        type: 'tweets',
        body: {
            from: 0,
            size: limit,
            query: {
                bool: {
                    filter: [{   
                            "range": {
                                "timestamp": {
                                    "lte": start_time
                                }
                            }
                        }
                    ]
                }
            }
        }
    }; 

    users_filter = []

    if (req_username) {
        users_filter.push(req_username);
    }

    // if follows list, add terms filter for the names
    if (following && follows.length > 0) {
        users_filter = users_filter.concat(follows);
    }

    if (users_filter.length > 0) {
        search_arg.body.query.bool.filter.push({ terms: { 'username': users_filter } });
    }

    // if query string, add a 'must match' for the string
    if (query_string) {
        search_arg.body.query.bool.must = [{ match: {'content': query_string }}];
    }

    if (!replies) {
        search_arg.body.query.bool.must_not = { match: { 'childType': 'reply' } }
    }

    if (hasMedia) {
        let bool_must = search_arg.body.query.bool.must;
        let must = [{ match: { exists: { field: 'media' } } }];

        if (bool_must) {
            bool_must.push(must[0]);
        }
        else {
            bool_must = must;
        }
    }

    console.log(JSON.stringify(search_arg));

    let search_res = undefined;
    try {
        search_res = await ElasticSearch.search( search_arg );
    } catch (err) {
        sendErr(res, err, 500);
        return;
    }   
    
    // return just array of the tweets from the search result
    let items = Array.from(search_res.hits.hits, function(obj) {
        return {
            id: obj._id,
            property: obj._source.property,
            retweeted: obj._source.retweeted,
            content: obj._source.content,
            timestamp: obj._source.timestamp,
            username: obj._source.username
        };
    });

    // yea havent done the interest part yet 
    items.sort((a, b) => {
        return a.timestamp - b.timestamp;
    });

    res.status(200).send({status: 'OK', items: items});
}

module.exports.search = search;

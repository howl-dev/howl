#! /bin/bash

sudo curl -XDELETE "localhost:9200/mongodb_meta?pretty"
sudo curl -XPOST "localhost:9200/$1/_delete_by_query?pretty" -H 'Content-Type: application/json' -d'{ "query": { "match_all": {} } } '
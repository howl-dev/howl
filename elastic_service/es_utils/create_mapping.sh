#! /bin/bash

sudo curl -XPUT 'localhost:9200/howldb?pretty' -H 'Content-Type: application/json' -d'
{
    "settings" : {
        "number_of_shards" : 3
    },
    "mappings" : {
        "tweets": {
            "properties" : {
                "username": { "type": "keyword" },
                "property": {
                    "type": "nested",
                    "properties": { 
                        "likes": { "type": "integer" } 
                    }
                },
                "retweeted": { "type": "integer" },
                "content": { "type": "text" },
                "timestamp": { "type": "long" },
                "uuid": { "type": "text" }
            }
        },
        "followers": {
            "properties" : {
                "parentUsername": { "type": "keyword" },
                "followerUsername": { "type": "keyword" }
            }
        }
    }
}
'

curl -XPOST localhost:9200/howldb/_refresh
const elasticsearch = require('elasticsearch');

const ElasticSearch = new elasticsearch.Client({
    host: process.env.ELASTIC_SEARCH,
});

module.exports.ElasticSearch = ElasticSearch;
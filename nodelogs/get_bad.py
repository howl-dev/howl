import numpy as np
import re
import sys

init_char = 't'
range_max = 8
if len(sys.argv) > 1:
	init_char = sys.argv[1]
if len(sys.argv) > 2:
	range_max = int(sys.argv[2])

logs = [f'{init_char}.log']

for i in range(1, range_max):
    logs.append(f'{init_char}{i}.log')

combined_lines = []

for f in logs:
    with open(f, "r") as file:
        total = [x.rstrip() for x in file.readlines()]
        combined_lines = combined_lines + total

print(f'Analyized {len(combined_lines)} lines')
ms_array = []

for x in combined_lines:
	try:
		if x[0][0] == '{':
			continue
		ms_array.append(float(x.split(' ')[3]))
	except Exception:
		continue

for line in combined_lines:
    line = line.split(' ')
    if not line or line[0] == '':    
        continue
    if line[0][0] == '{':
        continue
    if line[3] == '-':
        print(line)
        continue
#    if float(line[3]) > 200:
#        print(line)

np_arr = np.array(ms_array)
print(np.percentile(np_arr, 99))

const db = require('../db');
const memcached = require('../sessions');
const helpers = require('../helpers');
const jwt = require('jsonwebtoken');
const sendErr = helpers.sendErr;

async function followers(req, res) {
    let limit = req.body.limit || 50;
    if (limit > 200) limit = 200;

    let uname = req.params.username;
    if (!uname) {
        sendErr(res, 'No username provided', 401);
        return;
    }

    try {
        let follows = await db.Follower.find({
            parentUsername: uname
        }).select('followerUsername').limit(limit).lean().exec();

        let follow_squash = [];
        for (let i = 0; i < follows.length; i++) {
            follow_squash.push(follows[i].followerUsername);
        }

        res.status(200).send({ status: 'OK', users: follow_squash });
    } catch (err) {
        sendErr(res, err, 404);
    }
}

module.exports.followers = followers;

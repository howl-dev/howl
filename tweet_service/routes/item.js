const db = require('../db');
const memcached = require('../sessions');
const helpers = require('../helpers');
const jwt = require('jsonwebtoken');
const sendErr = helpers.sendErr;

async function getitem(req, res) {
    let ids = req.params.id;

    try {
        let tweet = await db.Tweet.find().where('uuid')
            .equals(ids).lean().exec();
        tweet = tweet[0];

        res.status(200).send({status: 'OK', 
            item: {
                id: ids,
                username: tweet.username,
                property: tweet.property,
                retweeted: tweet.retweeted,
                content: tweet.content,
                timestamp: tweet.timestamp,
                childType: tweet.childType,
                parent: tweet.parent,
                media: tweet.media
            }
        });
    } catch (err) {
        sendErr(res, err, 404);
    }
}

module.exports.getitem = getitem;
const db = require('../db');
const memcached = require('../sessions');
const helpers = require('../helpers');
const jwt = require('jsonwebtoken');
const sendErr = helpers.sendErr;
const fs = require('fs');
const MEDIA_PATH = helpers.MEDIA_PATH;

async function deleteitem(req, res) {
    if (!req.cookies || !req.cookies.key) {
        sendErr(res, 'No session found', 401);
        return;
    }

    let id = req.params.id;
    let jwtCookie = req.cookies.key;
    let payload = undefined;

    if (!id) {
        sendErr(res, 'No id sent', 400);
        return;
    }

    // make sure it's valid
    try {
        payload = await jwt.verify(jwtCookie, process.env.JWT_SIGN_SECRET);
    } catch(err) {
        res.clearCookie('key');
        sendErr(res, err, 401);
        return;
    }

    // make sure that the cookies are the same as the memcached
    // try {
    //     let chk = await memcached.retrieveSession(payload.key);
    //     if (chk != jwtCookie) throw 'Bad cookie';
    // } catch (err) {
    //     res.clearCookie('key');
    //     sendErr(res, err, 401);
    //     return;
    // }

    res.status(200).send({status: 'OK'});
    try {
        let rc = await db.Tweet.findOneAndRemove({uuid: id});
        if(rc.media) {
            for (let mediaId of rc.media) {
                fs.unlink(MEDIA_PATH + mediaId, () => {});
            }
        }
    } catch (err) {
        return;
    }
}

module.exports.deleteitem = deleteitem;
const db = require('../db');
const memcached = require('../sessions');
const helpers = require('../helpers');
const jwt = require('jsonwebtoken');
const crypto = require('crypto');
const sendErr = helpers.sendErr;

// RFC4122 compliant random function using crypto
function idgen() {
	return ([1e7]+-1e3+-4e3+-8e3+-1e11)
		.replace(/[018]/g, 
			c => (c ^ crypto.randomBytes(1)[0] & 15 >> c / 4)
				.toString(16))
}

async function additem(req, res) {
    if (!req.cookies || !req.cookies.key) {
        sendErr(res, 'No session found', 401);
        return;
    }

    let newContent = req.body.content;
    let childType = req.body.childType;
    let parent = req.body.parent;
    let media = req.body.media;
    let jwtCookie = req.cookies.key;
    let payload = undefined;

    if (!newContent) {
        sendErr(res, 'No content sent', 400);
        return;
    }

    // make sure it's valid
    try {
        payload = await jwt.verify(jwtCookie, process.env.JWT_SIGN_SECRET);
    } catch(err) {
        res.clearCookie('key');
        sendErr(res, err, 401);
        return;
    }

    // make sure that the cookies are the same as the memcached
    // try {
    //     let chk = await memcached.retrieveSession(payload.key);
    //     if (chk != jwtCookie) throw 'Bad cookie';
    // } catch (err) {
    //     res.clearCookie('key');
    //     sendErr(res, err, 401);
    //     return;
    // }

    // if childtype is retweet, then inc the retweeted count of the parent
    if (childType == 'retweet') {
        db.Tweet.findOneAndUpdate({uuid: parent}, { $inc: { retweeted: 1 }}).catch(()=>{});
    }
    
    let tweet_uuid = idgen();
    res.status(200).send({status: 'OK', id: tweet_uuid});

    try {
        let tweet = await new db.Tweet({
            username: payload.key, 
            property: { likes: 0 },
            retweeted: 0, 
            content: newContent,
            childType: childType,
            parent: parent,
            media: media,
            uuid: tweet_uuid
        }).save();
    } catch (err) {
        console.log('TWEET FAILED TO WRITE ' + err);
    }
}

module.exports.additem = additem;

const memcached = require('../sessions');
const jwt = require('jsonwebtoken');
const helpers = require('../helpers');
const sendErr = helpers.sendErr;
const MEDIA_PATH = helpers.MEDIA_PATH;
const fs = require('fs');

async function addmedia(req, res) {
    if (!req.cookies || !req.cookies.key) {
        sendErr(res, 'No session found', 401);
        return;
    }

    let jwtCookie = req.cookies.key;
    let payload = undefined;

    // make sure it's valid
    try {
        payload = await jwt.verify(jwtCookie, process.env.JWT_SIGN_SECRET);
    } catch(err) {
        res.clearCookie('key');
        sendErr(res, 'Not logged in', 401);
        return;
    }

    // make sure that the cookies are the same as the memcached
    // try {
    //     let chk = await memcached.retrieveSession(payload.key);
    //     if (chk != jwtCookie) throw 'Bad cookie';
    // } catch (err) {
    //     res.clearCookie('key');
    //     sendErr(res, err, 401);
    //     return;
    // }
    
    let content = req.file.buffer;
    let timestamp = new Date().getTime().toString();
    // attempt to add some more uniqueness to the filename
    let filename = timestamp + '.' + req.file.originalname;
    let path = MEDIA_PATH + filename;

    res.status(200).send({
        'status': 'OK',
        'id': filename
    });

    fs.writeFile(path, content, function(err) {
        if (err)
            console.log(path, 'failed to write.');
    });

}

module.exports.addmedia = addmedia;
const db = require('../db');
const memcached = require('../sessions');
const helpers = require('../helpers');
const jwt = require('jsonwebtoken');
const sql = require('../mariadb');
const sendErr = helpers.sendErr;

async function follow(req, res) {
    if (req.body.follow !== null || req.body.follow !== undefined) {
        if (!req.body.follow) {
            res.status(200).send({status: 'OK'});
        }
    }

    let uname = req.body.username;
    if (!uname) {
        sendErr(res, 'No username provided', 401);
        return;
    }

    // make sure they're logged in
    let jwtCookie = req.cookies.key;
    let myusername = undefined;
    try {
        myusername = await jwt.verify(jwtCookie, process.env.JWT_SIGN_SECRET);
        myusername = myusername.key;
    } catch (err) {
        res.clearCookie('key');
        sendErr(res, err, 401);
        return;
    }

    // try {
    //     let mcChk = await memcached.retrieveSession(myusername);
    //     if (mcChk != jwtCookie) throw 'Bad cookie';
    // } catch (err) {
    //     res.clearCookie('key');
    //     sendErr(res, err, 401);
    //     return;
    // }
    
    res.status(200).send({status: 'OK'});
    // make sure the user they're trying to follow exists
    let dbres = await sql.User.find({
        where: {
            name: uname
        }
    });

    if (!dbres) {
        // sendErr(res, 'User does not exist!', 401);
        return;
    }

    try {
        await new db.Follower({ parentUsername: uname,
            followerUsername: myusername }).save();
        // res.status(200).send({status: 'OK'});
    } catch (err) {
        // sendErr(res, err, 500);
    }
}

module.exports.follow = follow;
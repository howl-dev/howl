const express = require('express');
const router = express.Router();
const multer = require('multer');

/* Multer middleware for addmedia endpoint */
const storage = multer.memoryStorage();
const upload = multer({
    storage: storage
});

/* Endpoint Modules */
const additem = require('./additem');
const item = require('./item');
const deleteitem = require('./deleteitem');
const follow = require('./follow');
const following = require('./following');
const followers = require('./followers');
const like = require('./like');
const addmedia = require('./addmedia');

/* Endpoints */
router.post('/additem', additem.additem);
router.get('/item/:id', item.getitem);
router.delete('/item/:id', deleteitem.deleteitem);
router.post('/follow', follow.follow);
router.get('/user/:username/following', following.following);
router.get('/user/:username/followers', followers.followers);
router.post('/item/:id/like', like.like);
router.post('/addmedia', upload.single('content'), addmedia.addmedia);

module.exports = router;
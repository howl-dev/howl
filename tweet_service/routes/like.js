const db = require('../db');
const memcached = require('../sessions');
const helpers = require('../helpers');
const jwt = require('jsonwebtoken');
const sendErr = helpers.sendErr;

async function like(req, res) {
    let id = req.params.id;
    let like = req.body.like;
    let inc = like !== undefined ? (like ? 1 : -1) : 1;

    try {
        await db.Tweet.findOneAndUpdate({uuid: id}, {
            $inc : {
                'property.likes': inc
            }
        });

        res.status(200).send({
            status: 'OK'
        });
    } catch(err) {
        return;
    }
}

module.exports.like = like;
/* eslint-disable */
let memcached = require('mc');
let MemCached = new memcached.Client(process.env.MEMCACHED_SESSION_URLS,
    memcached.Adapter.json, memcached.Strategy.hash);

MemCached.connect(()=>{
    console.log('Service succesfully contacted MemCached cluster.');
});
    // {
    //     retries: 5, // retry a maximum of 5 times
    //     maxExpiration: 60 * 60 * 24 * 7, // 7 days maxExpir 
    //     timeout: 1000 * 60 * 1, // timeout after connection dropped >1min
    //     failrues: 5,        // 5 failures die
    //     retry: 1000 * 5,    // 5s retry
    //     idle: 1000 * 60 * 30 // 30 min timeout
    // });

// The next set of functions are designed to promisify the API

/**
 * Retrieves the value for a given key via a memcached server
 * @param {any} key The key for the user, typically the username
 * @returns A Promise with (Error, Result).
 */
function retrieveSession(key) {
    return new Promise((resolve, reject) => {
        MemCached.get(key, function(err, res){
            if (err) reject(err);
            if (!res) resolve('');
            else if (!res[key]) resolve('');
            else resolve(res[key]['val']);
        })
    });
}

/**
 * Creates a session with key: key and value: value
 * @param {any} key The key for the user, typically the username
 * @param {any} value The value to store for the user as a session
 * @returns A Promise with (Err). Note: Err happens when a session already exists
 */
function createSession(key, value) {
    return new Promise((resolve, reject) => {
        MemCached.add(key, value, 5, function(err) {
            if (err) reject(err);
            resolve();
        })
    });
}

/**
 * Updates the session of key with value
 * @param {any} key The key for the user, typically the username
 * @param {any} value The value to store for the user as a session
 * @returns A Promise with (Err)
 */
function updateSession(key, value) {
    return new Promise((resolve, reject) => {
        MemCached.set(key, value, { flags: 0, exptime: 0 }, function(err){
            if (err) reject(err);
            resolve();
        })
    });
}

/**
 * Destroys a given key in the memcached cluster
 * @param {any} key The key for the user, typically the username
 */
function destroySession(key) {
    return new Promise((resolve, reject) => {
        MemCached.del(key, function(err){
            if (err) reject(err);
            resolve();
        })
    });
}


module.exports.MemCached = MemCached;
module.exports.createSession = createSession;
module.exports.retrieveSession = retrieveSession;
module.exports.updateSession = updateSession;
module.exports.destroySession = destroySession;
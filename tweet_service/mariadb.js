var Sequelize = require('sequelize');

/* Init MariaDB connection */
var sequelize = new Sequelize('usersdb', 'ubuntu', 'cse356', {
    host: process.env.SQL_DB_URL,
    dialect: 'mysql',
    logging: false,
    pool: {
        max: 100
    }
});

/* Connect */
sequelize.authenticate()
    .catch(function(err) {
        console.log('Could not connect to DB. ' + err);
    });

/* User model */
const User = sequelize.define('user', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        unique: true,
    },
    name: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    email: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    // salt: { },
    // TODO update password to salt, for now use plain password
    password: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    verified: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
        allowNull: false,
    },
    key: {
        type: Sequelize.STRING,
        allowNull: false,
    }
});

User.sync();

module.exports.User = User;
module.exports.Sequelize = Sequelize;
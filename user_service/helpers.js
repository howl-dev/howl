/**
 * Sends Error to res object
 * @param {Response} res Response object from calling scope
 * @param {any} err Error object from Promise
 */
function sendErr(res, err, code) {
    res.json({status: 'error', err: err});
    res.status(code ? code : 500).send();
}

module.exports.sendErr = sendErr;
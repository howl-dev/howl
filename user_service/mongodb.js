const mongoose = require('mongoose');

/* Connect to cluster */
mongoose.connect('mongodb://' + process.env.CLUSTER_HOST + '/howldb', (err, doc) => err ? console.log(err) : {});
let Schema = mongoose.Schema;

const TweetSchema = new Schema({
    username: String,
    property: {
        likes: {type: Number, default: 0}
    },
    retweeted: {type: Number, default: 0},
    content: String,
    timestamp: { type: Number, default: +new Date() },
    childType: String,
    parent: String,
    uuid: String,
    media: []
}, {collection: 'tweets'});

const FollowerSchema = new Schema({
    parentUsername: String,
    followerUsername: String
}, {collection: 'followers'});

module.exports.mongoose = mongoose;
module.exports.Tweet = mongoose.model('TweetModel', TweetSchema);
module.exports.Follower = mongoose.model('FollowerModel', FollowerSchema);
const db = require('../db');
const mongo = require('../mongodb');
const helpers = require('../helpers');
const sendErr = helpers.sendErr;

async function userinfo(req, res) {
    let uname = req.params.username;
    if (!uname) {
        res.status(200).send({status: 'OK', user: { email: null, followers: 0, following: 0}});
        return;
    }

    let dbres = await db.User.find({
        attributes: 'email',
        where: {
            name: uname
        }
    });

    if (!dbres) {
        sendErr(res, 'User not found', 401);
        return;   
    }

    // this is probably over reaching and should just talk to the other service
    let followers = await mongo.Follower.count({ parentUsername: uname }); // maybe aggregate these to one?
    let following = await mongo.Follower.count({ followerUsername: uname });

    res.status(200).send({status: 'OK', user: {
        'email': dbres,
        'followers': followers,
        'following': following
    }});
}

module.exports.userinfo = userinfo;
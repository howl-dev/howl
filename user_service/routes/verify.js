const db = require('../db');
const helpers = require('../helpers');
const sendErr = helpers.sendErr;

const backdoor = 'abracadabra';

/**
 * POST handler for '/verify' endpoint. Verifies the user and updates the
 * verified field in the DB.
 * @param {Request} req 
 * @param {Response} res 
 */
async function verify(req, res) {
    let email = req.body.email;
    let key = req.body.key;
    let user = undefined;

    let sql_query = {
        where: {
            email:email
        }
    };

    if (!(key === backdoor)) {
        sql_query.where.key = key;
    }

    try {
        user = await db.User.findOne(sql_query);
    } catch(err) {
        sendErr(res, err);
        return;
    }

    if (!user) {
        helpers.sendErr(res, 'Can not verify user', 401);
        return;
    }

    if (user.verified) {
        res.json({status:'OK'});
        res.status(200).send();
        return;
    }

    try {
        await user.update({
            verified: true
        });
    } catch(err) {
        sendErr(res, err);
        return;
    }

    res.json({status:'OK'});
    res.status(200).send();
}

module.exports.verify = verify;
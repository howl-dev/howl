const memcache = require('../sessions');
const sendErr = require('../helpers').sendErr;
const jwt = require('jsonwebtoken');

/**
 * Logout a user
 * clear memcache session for this user's session and clear their cookie.
 * @param {Request} req 
 * @param {Response} res 
 */
async function logout(req, res) {
    if (!req.cookies || !req.cookies.key) {
        res.json({status: 'OK'});
        res.status(200).send();
        return;
    }

    let cookie = req.cookies.key;
    let payload = undefined;

    try {
        payload = await jwt.verify(cookie, process.env.JWT_SIGN_SECRET);
    } catch(err) {
        res.clearCookie('key');
        sendErr(res, err, 401);
        return;
    }

    // make sure that the cookies are the same as the memcached
    // try {
    //     let chk = await memcache.retrieveSession(payload.key);
    //     if (chk != payload) {
    //         res.clearCookie('key').status(200).send({status: 'OK'});
    //         return;
    //     }
    // } catch (err) {
    //     res.clearCookie('key');
    //     sendErr(res, err, 401);
    //     return;
    // }

    // let val = await memcache.destroySession(payload.key)
    //     .catch(()=>{}); // don't care if it fails

    res.clearCookie('key').status(200).send({status: 'OK'});
}

module.exports.logout = logout;
const db = require('../db');
const nodemailer = require('nodemailer');
const Op = db.Sequelize.Op;
const helpers = require('../helpers');
const sendErr = helpers.sendErr;

/**
 * Post request for '/adduser' endpoint. Creates a user if they do not exist,
 * and sends a verification email.
 * @param {Request} req Request object
 * @param {Response} res Response object
 */
async function adduser(req, res) {
    let username = req.body.username;
    let email = req.body.email;
    let password = req.body.password;
    let verifKey = Math.random().toString(36).substring(2);
    let user = undefined;
    let bd = req.body.bd;
    // verify that this user does not already exist
   
    try {
        user = await db.User.findOne({
            where: {
                [Op.or]: [
                    {name: username},
                    {email: email},
                ]
            }
        });
    } catch(err) {
        sendErr(res, err);
        return;
    }

    if (user) {
        sendErr(res, 'User with these credentials already exists.', 400);
        return;
    }

    // user does not exist, add to db.
    try {
        await db.User.build({
            name: username,
            email: email,
            password: password,
            key: verifKey,
            verified: bd == 1 ? 1 : 0
        }).save();
    } catch(err) {
        sendErr(res, err);
        return;
    }

    if (bd) {
        res.status(200).send({status: 'OK'});
        return;
    }

    // transport object
    let transport = nodemailer.createTransport({
        host: 'localhost',
        port: 25,
        tls: {
            rejectUnauthorized: false
        }
    });


    // init mail object
    let mail = {
        from: '"Howl Team" sbucompaslab@gmail.com',
        to: email,
        subject: 'Howl Verification Email',
        text: `validation key: <${verifKey}>`
    };

    // send the mail and response
    while(1) {
        try { 
            await transport.sendMail(mail);
            res.status(200).send({ status: 'OK' });
            break;
        } catch (e) {
            console.log(e);
        }
        
        await new Promise(function(a, r) {
            setTimeout(a, 20);
        });
    } 
}

module.exports.adduser = adduser;
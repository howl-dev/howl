/**
 * GET handler for root index '/'
 * @param {any} req Request object
 * @param {any} res Response object 
 */
function root(req, res) {
    res.json({message: 'hello'});
    res.status(200).send();
}

module.exports.root = root;
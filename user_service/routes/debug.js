const memcache = require('../sessions');
const helpers = require('../helpers');
const sendErr = helpers.sendErr;

/**
 * Used for debuging the values inside the memcache cluster
 * @param {Request} req 
 * @param {Response} res 
 */
async function getDataFromCache(req, res) {
    let key = req.body.key;
    let val = await memcache.retrieveSession(key)
        .catch(e => res.status(404).send(e));
    res.status(200).send(val);
}

module.exports.getDataFromCache = getDataFromCache;
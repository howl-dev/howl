const express = require('express');
const router = express.Router();
const memcache = require('../sessions');

/* Endpoint Modules */
const root = require('./root');
const adduser = require('./adduser');
const verify = require('./verify');
const login = require('./login');
const logout = require('./logout');
const debug = require('./debug');
const userinfo = require('./userinfo');

/* Endpoints */
router.get('/', root.root);
router.get('/user/:username', userinfo.userinfo);
router.post('/adduser', adduser.adduser);
router.post('/verify', verify.verify);
router.post('/login', login.login);
router.post('/logout', logout.logout);
router.post('/debug_sess', debug.getDataFromCache);

module.exports = router;
const db = require('../db');
const memcache = require('../sessions');
const helpers = require('../helpers');
const jwt = require('jsonwebtoken');
const sendErr = helpers.sendErr;

/**
 * Login a user
 * Verify username and password, add key value pair (username + 'salt', id)
 * to memcached session store and send cookie of that key
 * @param {Request} req Request object
 * @param {Response} res Response object
 */
async function login(req, res) {
    let name = req.body.username;
    let password = req.body.password; // TODO update to salted pw
    let user = undefined;

    try {
        user = await db.User.findOne({
            where: {
                name: name,
                password: password,
                verified: true
            }
        });
    } catch(err) {
        console.log(`[CRITICAL] User DB Error. ${err.message}`);
        sendErr(res, 'Internal User Database Error. Try again later.');
        return;
    }

    if(!user) {
        sendErr(res, 'Not a valid account', 400);
        return;
    }

    let payload = {
        'key': user.name,
        'id': user.id
        // put more fields here when needed
    };

    // make the key the username
    let key = user.name;
    // the value the jwt label

    let token = jwt.sign(payload, process.env.JWT_SIGN_SECRET);
    // don't care if it doesnt work
    // let val = await memcache.destroySession(key).catch(()=>{});

    // try {
    //     await memcache.createSession(key, token);
    // } catch (err) {
    //     console.log(`[CRITICAL] Memcached Error | ${err.type}: ${err.description}`);
    //     sendErr(res, 'Cannot login.');
    //     return;
    // } 

    res.cookie('key', token).status(200).send({status: 'OK'});
}

module.exports.login = login;